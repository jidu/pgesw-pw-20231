// call useState from component to store state/data.
import { useState, FunctionComponent, ReactNode, ReactElement } from 'react';


interface SquareProps {
  value: string;
  onSquareClick: () => void;
};

// Child component.
function Square({ value, onSquareClick }: SquareProps): ReactElement {
  return (
    <button className="square" onClick={onSquareClick}>
      {value}
    </button>
  );
}


interface BoardProps {
  xIsNext: boolean;
  squares: Array<string>;
  onPlay: (nextSquares: Array<string>) => void;
}


/**
 * Parent component.
 * Export as main feature/Component of this module.
 * Can then just import App from App.tsx and use as the component <App /> instead of <Board />
 * @param {(nextSquares: Array<string>) => void} onPlay Call with updated squares array when player makes a move.
 * @return {} 
 */
function Board({ xIsNext, squares, onPlay }: BoardProps): ReactElement {
  // To remember that user clicked square.
  // Will pass as prop to child component Square.
  // Side-effect: triggers re-render of components that use sqquares state (Board) 
  // and its child components (Square components) using copy of the squares to set it again.
  function handleClick(i: number) {
    // Check if square is filled or there is a winner already.
    if (Boolean(squares[i]) || calculateWinner(squares)) {
      return;
    }

    // Create copy of squares array.
    // IMMUTABILITY: using copy for immutability purposes as it allows keeping previous states for use.
    // React compares next and pervious, and only re-renders what changed instead of everything.
    const nextSquares: typeof squares = squares.slice();

    // Update nextSquares array to add X to i.
    // So that Reacts know there is change in component state.
    if (xIsNext) {
      nextSquares[i] = "X";
    } else {
      nextSquares[i] = "O";
    }
    onPlay(nextSquares);
  }

  // Status board.
  const winner: (string | undefined) = calculateWinner(squares);
  let status: string;
  if (winner === undefined) {
    status = "Next player: " + (xIsNext ? "X" : "O");
  } else {
    status = "Winner: " + winner;
  }

  // className="square" is a button property (prop) from index.css 
  // which tells CSS how to style the button.
  // Use arrow function to ensure calling handleClick only onClick event 
  // instead of right away during rendering.
  return (
    <>
      <div className="status">{status}</div>
      <div className="board-row">
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
      </div>
      <div className="board-row">
        <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
        <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
        <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
      </div>
      <div className="board-row">
        <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
        <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
        <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
      </div>
    </>
  );
}


// export default for index.ts to use Game as top-level component.
export default function Game(): ReactElement {
  // squares stores stateful value in 9 arrays, initally ''. setSquares is function to change values.
  // Needs to remember all 9 squares states. Share state in parent component Board instead of child component Square
  // for better parent->child control, collecting data from multiple children, multiple child intercommunication.
  // So that parent component can pass state back down to children via props. 
  // This keeps child components in sync with each other and their parent.
  const [history, setHistory]: [Array<Array<string>>, any] = useState([Array(9).fill('')]);
  const [currentMove, setCurrentMove]: [number, any] = useState(0);

  // To flip X or O player turn.
  const xIsNext: boolean = currentMove % 2 === 0;
  
  // Game component shall render only currently selected move, instead of final move.
  const currentSquares: Array<string> = history[currentMove];

  // Board child component calls handlePlay to update game.
  function handlePlay(nextSquares: Array<string>) {
    // When moving back, only keep the history up to that point.
    const nextHistory: [...Array<Array<string>>, Array<string>] = [...history.slice(0, currentMove + 1), nextSquares];
    // ... expands/unest history array elements to include in the beginning of history and nextSquares, as a new array.
    setHistory(nextHistory);

    // Update currentMove to latest history entry each time player makes a move.
    setCurrentMove(nextHistory.length - 1);
  }

  function jumpTo(nextMove: number) {
    setCurrentMove(nextMove);
  }

  const moves: Array<ReactElement> = history.map((squares, move) => {
    let description: string;
    if (move > 0) {
      description = "Go to move#" + move;
    } else {
      description = "Go to game start";
    }

    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>
          {description}
        </button>
      </li>
    );
  });

  return (
    <div className="game">
      <div className="game-board"><Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} /></div>
      <div className="game-info">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}


/**
 * Checks for solved game, winner.
 * @param {Array<string>} squares Grid
 * @return {(string | undefined)} Winner, i.e. X, or O, or none
 */
function calculateWinner(squares: Array<string>): (string | undefined) {
  const linesToWin: Array<Array<number>> = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ]

  for (let i = 0; i < linesToWin.length; i++) {
    const [a, b, c]: Array<number> = linesToWin[i];

    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }

  return undefined;
}
