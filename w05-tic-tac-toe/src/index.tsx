// Bridge between component from App file and the Web browser.

// React.
import React, { StrictMode } from "react";
// React's library to communicate to web browsers (React DOM).
import { createRoot } from "react-dom/client";
// CSS for components.
import "./index.css";
// My component.
import App from "./App";

// Brings all pieces together AND injects final product into public/index.html.
const root = createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <StrictMode>
    <App />
  </StrictMode>
);