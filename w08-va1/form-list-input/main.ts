function createListItem(): HTMLElement {
  const formInput: HTMLElement = document.getElementById("form-input");

  // https://code.whatever.social/questions/12989741/the-property-value-does-not-exist-on-value-of-type-htmlelement#12990166
  let listItem: HTMLElement = document.createElement("li");
  listItem.innerHTML = (<HTMLInputElement>formInput).value;

  return listItem;
}

function addListItemToList(list: HTMLElement, listItem: Node): void {
  list.appendChild(listItem);

  // document.getElementById("unordered-list")?.appendChild(createListItem());
}

function clearList(list: HTMLElement): void {
  list.innerHTML = "";
}

function main(): void {
  const buttonAdd: HTMLElement = document.getElementById("button-add");
  const buttonClearAll: HTMLElement = document.getElementById("button-clear-all");
  let unorderedList: HTMLElement = document.getElementById("unordered-list");

  // `() =>` ensures evaluating `createListItem` at the time of clicking the
  // button, instead of when creating the event listener.a
  buttonAdd.addEventListener("click", () => addListItemToList(unorderedList, createListItem()));
  buttonClearAll.addEventListener("click", () => clearList(unorderedList));
}

main();
