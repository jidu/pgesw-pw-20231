function createListItem() {
    var formInput = document.getElementById("form-input");
    // https://code.whatever.social/questions/12989741/the-property-value-does-not-exist-on-value-of-type-htmlelement#12990166
    var listItem = document.createElement("li");
    listItem.innerHTML = formInput.value;
    return listItem;
}
function addListItemToList(list, listItem) {
    list.appendChild(listItem);
    // document.getElementById("unordered-list")?.appendChild(createListItem());
}
function clearList(list) {
    list.innerHTML = "";
}
function main() {
    var buttonAdd = document.getElementById("button-add");
    var buttonClearAll = document.getElementById("button-clear-all");
    var unorderedList = document.getElementById("unordered-list");
    // `() =>` ensures evaluating `createListItem` at the time of clicking the
    // button, instead of when creating the event listener.a
    buttonAdd.addEventListener("click", function () { return addListItemToList(unorderedList, createListItem()); });
    buttonClearAll.addEventListener("click", function () { return clearList(unorderedList); });
}
main();
